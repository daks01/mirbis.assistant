SiteAssistant = (function (module) {
    'use strict';
    var modal,
        modalFullscreen,
        modalAside,
        modalBottom,
        modalCloseBtn,
        optionFullscreen,
        optionAside,
        optionBottom,
        getObjectName,
        hasObject,
        isTrueObject,
        addObject,
        removeObject,
        showModal,
        closeModal,
        bindEvents,
        module = function () {};
    
    hasObject = function (key) {
        return localStorage.getItem(key) ? true : false;
    }
    
    addObject = function (key) {
        localStorage.setItem(key, new Date());
    } 
    
    removeObject = function (key) {
        localStorage.removeItem(key);
    } 
    
    showModal = function () {
        modal.fadeOut();
        if(!hasObject(optionFullscreen)) {
            modalFullscreen.show().addClass('animated pulse');  //flipInX
            removeObject(getObjectName(modalFullscreen));
        } else if (!hasObject(optionBottom) || hasObject(optionAside)) {
            modalBottom.show().addClass('animated bounce');
            removeObject(getObjectName(modalBottom));
            removeObject(getObjectName(modalAside));
        } else {
            modalAside.show().addClass('animated flipInX');
            removeObject(getObjectName(modalAside));
        }
    }
    
    closeModal = function (el) {
        var parentModal = el.closest(modal);
        addObject(getObjectName(parentModal));
        showModal();
    }
    
    getObjectName = function (el) {
        return el.data('assistant-modal');
    };
    
    bindEvents = function () {
        showModal();
        
        modalCloseBtn.click(function(){
            closeModal($(this))
        });
    }

    module.init = function () {
        modal = $('[data-assistant-modal]');
        modalFullscreen = modal.filter('[data-assistant-modal=site-asistant-close-fullscreen]');
        modalAside = modal.filter('[data-assistant-modal=site-asistant-close-aside]');
        modalBottom = modal.filter('[data-assistant-modal=site-asistant-close-bottom]');
        modalCloseBtn = modal.find('[data-assistant-modal-close]');
        optionFullscreen = getObjectName(modalFullscreen);
        optionAside = getObjectName(modalAside);
        optionBottom = getObjectName(modalBottom);

        bindEvents();
    }

    return module;

})();