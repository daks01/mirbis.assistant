SiteAssistantDialog = (function (module) {
    'use strict';
    var modal,
        generators,
        generate,
        options,
        dialog,
        defaultOptions = {},
        bindEvents,
        module = function () {};

    generators = {
        form: function(options){
            return '<div class="site-assistant__dialog-item"><div class="input-group"><input type="text" class="form-control" placeholder="+7 ### ### ## ##"><span class="input-group-btn"><input value="' + options.submit + '" class="btn btn-default js-dialog-callback" type="submit"></span></div><div class="h3 js-dialog-callback-succes" style="display: none;">' + options.success + '</div></div>';
        },
        link: function(options){
            return '<div class="site-assistant__dialog-item"><a href="' + options.href + '" class="site-assistant__dialog-link">' + options.title + '</a></div>';
        },
        button: function(options){
            return '<div class="site-assistant__dialog-item"><span data-action="' + options.action + '" class="site-assistant__dialog-link js-dialog-btn">' + options.title + '</span></div>';
        }
    };
    
    generate = function (scenario) {
        var html_objects = '';
    
        $.each(scenario.objects, function(i, object){
            var generator = generators[object.type];
            html_objects += generator(object.options);
        });
        
        var $content = scenario.options.title !== undefined ? $('<div class="site-assistant__dialog-item h3">' + scenario.options.title + '</div>' + html_objects) : $(html_objects);

        $('[data-assistant-dialog-content]').html($content);
    };
    
    bindEvents = function () {
        generate(dialog['scenario']);

        $(document).on('click', '.js-dialog-btn', function(){
            generate(dialog[$(this).data('action')]);
            return false;
        });

        $(document).on('click', '.js-dialog-callback', function(){
            $('.js-dialog-callback-succes').show();
            return false;
        });
    }

    module.init = function (opt) {
        options = $.extend({}, defaultOptions, opt);
        dialog = options.dialog;
        
        if (dialog !== null && typeof dialog === 'object') {
            bindEvents();
        }
    }

    return module;

})();